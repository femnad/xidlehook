//! A module for checking if a webcam in use by running the equivalent of `fuser /dev/video*`.
use std::process::Command;
use crate::{Module, Progress, TimerInfo};

use glob::glob;

fn glob_video_devices() -> Result<Vec<String>, glob::PatternError> {
    let mut output: Vec<String> = Vec::new();
    match glob("/dev/video*") {
        Ok(paths) => {
            for path in paths {
                match path {
                    Ok(path) => {
                        output.push(path.into_os_string().into_string().unwrap());
                    }
                    Err(e) => {
                        eprintln!("{}", e);
                    }
                }
            }
            Ok(output)
        }
        Err(e) => {
            Err(e)
        }
    }
}

fn video_devices_in_use() -> bool {
    let video_devices = glob_video_devices();
    match video_devices {
        Ok(devices) => {
            let output = Command::new("fuser")
                .args(devices)
                .output();
            let out = output.unwrap();
            if out.status.success() {
                return true
            }
            let stderr = String::from_utf8_lossy(&out.stderr);
            if stderr.is_empty() {
                return false
            }
            eprintln!("{}", stderr);
        }
        Err(e) => {
            eprintln!("{}", e);
        }
    }
    false
}

/// Base struct for fuser_dev_video module.
#[derive(Clone, Copy, Debug)]
pub struct FuserDevVideo {}

impl FuserDevVideo {
    /// Create a new instance of this module.
    pub fn new() -> Self {
        Self {}
    }
}

impl Module for FuserDevVideo {
    fn pre_timer(&mut self, _timer: TimerInfo) -> crate::Result<Progress> {
        if video_devices_in_use() {
            return Ok(Progress::Abort);
        }

        Ok(Progress::Continue)
    }
}